<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
})->name('dashboard');

Route::prefix('diagnosa')->group(function() {
    //Diagnosa
    Route::get('new', 'DiagnosaController@index')->name('diagnosa');
    Route::get('create', 'DiagnosaController@create')->name('diagnosa.create');
    Route::post('', 'DiagnosaController@store')->name('diagnosa.store');
    Route::get('first/{id}', 'DiagnosaController@firstQuestion')->name('diagnosa.first');
    Route::get('pertanyaan/{id}/diagnosa/{pertanyaanId}/{isTrue}', 'DiagnosaController@executeQuestion')->name('diagnosa.question');
    Route::get('result/{id}/view', 'DiagnosaController@result')->name('diagnosa.result');
    Route::get('diagnosa/search', 'DiagnosaController@search')->name('diagnosa.search');
});

Route::prefix('master')->group(function() {
    //Master Kerusakan
    Route::get('kerusakan', 'Master\KerusakanController@index')->name('kerusakan');
    Route::get('kerusakan/search', 'Master\KerusakanController@search')->name('kerusakan.search');
    //Master Gejala
    Route::get('gejala', 'Master\GejalaController@index')->name('gejala');
    Route::get('gejala/search', 'Master\GejalaController@search')->name('gejala.search');
});
