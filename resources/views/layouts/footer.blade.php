<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 2.3.3
    </div>
    <strong>Copyright &copy; 2021 <a href="http://ars.ac.id/">Sistem Pakar Bengkel</a>.</strong> All rights
    reserved.
</footer>
