<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="{{ (request()->is('/')) ? 'active' : '' }}"><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
            <li class="{{ (request()->is('diagnosa')) ? 'active' : '' }}"><a href="{{ route('diagnosa') }}"><i class="fa fa-gear"></i> <span>Diagnosa</span></a></li>
            <li class="header">MASTER</li>
            <li class="{{ (request()->is('master/kerusakan')) ? 'active' : '' }}"><a href="{{ route('kerusakan') }}"><i class="fa fa-database"></i> <span>Kerusakan</span></a></li>
            <li class="{{ (request()->is('master/gejala')) ? 'active' : '' }}"><a href="{{ route('gejala') }}"><i class="fa fa-database"></i> <span>Gejala</span></a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
