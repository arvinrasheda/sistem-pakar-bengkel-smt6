@extends('layouts.app')

@section('pagetitle', 'Master Gejala')

@section('content')
    <section class="content-header">
        <h1>
            Master Gejala
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Master</a></li>
            <li class="active">Gejala</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-header">
                <div class="pull-right">
                </div>
            </div>
            <div class="box-body">
                <table id="gejala-table" class="table table-responsive table-condensed table-bordered"
                       style="width: 100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Kerusakan</th>
                        <th>Pertanyaan</th>
                        <th>Solusi</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@endsection


@section('js')
    <script type="text/javascript">
        $(function () {
            var table = $('#gejala-table').DataTable({
                sorting: false,
                iDisplayLength: 25,
                language: {
                    infoFiltered: ""
                },
                columns: [
                    {
                        searchable: false,
                        sortable: false,
                        data: null,
                        name: null,
                        render: function(data, type, row, meta){
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        sortable: false,
                        data: "kode_kerusakan"
                    },
                    {
                        sortable: false,
                        data: "pertanyaan"
                    },
                    {
                        sortable: false,
                        data: "solusi"
                    },
                ],
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('gejala.search') }}",
                }
            });
        } );
    </script>
@endsection
