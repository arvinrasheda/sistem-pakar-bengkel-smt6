@extends('layouts.app')

@section('pagetitle', 'Master Kerusakan')

@section('content')
    <section class="content-header">
        <h1>
            Master Kerusakan
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Master</a></li>
            <li class="active">Kerusakan</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-header">
                <div class="pull-right">
{{--                    <a href="{{ route('kerusakan.create') }}" class="btn btn-success btn-sm"><i--}}
{{--                            class="fa fa-plus"></i> Tambah</a><br>--}}
                </div>
            </div>
            <div class="box-body">
                <table id="kerusakan-table" class="table table-responsive table-condensed table-bordered"
                       style="width: 100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Kode</th>
                        <th>Nama</th>
{{--                        <th>Action</th>--}}
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@endsection


@section('js')
    <script type="text/javascript">
        $(function () {
            var table = $('#kerusakan-table').DataTable({
                sorting: false,
                iDisplayLength: 25,
                language: {
                    infoFiltered: ""
                },
                columns: [
                    {
                        searchable: false,
                        sortable: false,
                        data: null,
                        name: null,
                        render: function(data, type, row, meta){
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        sortable: false,
                        data: "kode"
                    },
                    {
                        sortable: false,
                        data: "nama"
                    },
                    {{--{--}}
                    {{--    data: null,--}}
                    {{--    name: null,--}}
                    {{--    sortable: false,--}}
                    {{--    render: function(data, type, row, meta){--}}
                    {{--        --}}{{--var view = '{{ route("billercategory.view", ":id") }}';--}}
                    {{--        --}}{{--view = view.replace(':id', row.id);--}}
                    {{--        --}}{{--var edit = '{{ route("billercategory.edit", ":id") }}';--}}
                    {{--        --}}{{--edit = edit.replace(':id', row.id);--}}
                    {{--        --}}{{--var hapus = '{{ route("billercategory.destroy", ":id") }}';--}}
                    {{--        --}}{{--hapus = hapus.replace(':id', row.id);--}}
                    {{--        --}}{{--return `--}}
                    {{--        --}}{{--<form action="${hapus}" method="post">--}}
                    {{--        --}}{{--        <input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
                    {{--        --}}{{--        <input type="hidden" name="list_id" value="${row.billercategoryid}">--}}
                    {{--        --}}{{--      <a href="${view}" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></a>--}}
                    {{--        --}}{{--      <a href="${edit}" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i></a>--}}
                    {{--        --}}{{--      <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Anda yakin akan menghapus data ' + '${row.name}' + ' ini ?');">--}}
                    {{--        --}}{{--          <i class="fa fa-trash"></i>--}}
                    {{--        --}}{{--      </button>--}}
                    {{--        --}}{{--</form>--}}
                    {{--        --}}{{--`;--}}
                    {{--    }--}}
                    {{--},--}}
                ],
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('kerusakan.search') }}",
                }
            });
        } );
    </script>
@endsection
