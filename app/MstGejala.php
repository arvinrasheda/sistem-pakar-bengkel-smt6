<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $kode
 * @property string $pertanyaan
 * @property int $bila_benar
 * @property int $bila_salah
 * @property string $solusi
 */
class MstGejala extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'mst_gejala';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * @var array
     */
    protected $fillable = ['kode_kerusakan', 'pertanyaan', 'bila_benar', 'bila_salah', 'solusi', 'is_first'];

    public function kerusakan()
    {
        return $this->hasOne(MstKerusakan::class, 'kode', 'kode_kerusakan');
    }

}
