<?php


namespace App\Http\Controllers\Master;


use App\Http\Controllers\Controller;
use App\MstGejala;

class GejalaController extends Controller
{
    public function index()
    {
        return view('master/gejala/index');
    }

    public function search()
    {
        try {
            return datatables()->of(MstGejala::all())->toJson();
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }
    }
}
