<?php


namespace App\Http\Controllers\Master;


use App\Http\Controllers\Controller;
use App\MstKerusakan;
use Illuminate\Support\Facades\DB;

class KerusakanController extends Controller
{
    public function index()
    {
        return view('master/kerusakan/index');
    }

    public function search()
    {
        try {
            return datatables()->of(DB::table('mst_kerusakan')->orderBy('kode')->get())->toJson();
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }
    }
}
