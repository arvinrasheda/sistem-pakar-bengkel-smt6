/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 100414
 Source Host           : localhost:3306
 Source Schema         : sistempakarbengkel

 Target Server Type    : MySQL
 Target Server Version : 100414
 File Encoding         : 65001

 Date: 02/07/2021 15:24:21
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for mst_gejala
-- ----------------------------
DROP TABLE IF EXISTS `mst_gejala`;
CREATE TABLE `mst_gejala`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `kode_kerusakan` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `pertanyaan` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `bila_benar` int NOT NULL,
  `bila_salah` int NOT NULL,
  `solusi` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_first` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mst_gejala
-- ----------------------------
INSERT INTO `mst_gejala` VALUES (1, 'K1', 'Suara Mesin Kasar?', 2, 6, 'usahakan ganti oli rutin dan selalu memerhatikan oktan bahan bakar yang akan di gunakan', 1);
INSERT INTO `mst_gejala` VALUES (2, 'K1', 'Apakah Mesin susah nyala?', 3, 6, 'usahakan tidak sampai kehabisan oli', 0);
INSERT INTO `mst_gejala` VALUES (3, 'K1', 'Apakah Terdengar kasar\r\npada bagian mesin?', 4, 6, 'perlu di cek dibagian seher apabila baret ada kemungkinan dari seher nya', 0);
INSERT INTO `mst_gejala` VALUES (4, 'K1', 'Pada saat di gas\r\nsuara mesin kasar\r\ndan menimbulkan\r\ngetaran\r', 5, 6, 'ganti bearing kruk as dan re center kruk as', 0);
INSERT INTO `mst_gejala` VALUES (5, 'K1', 'Terdapat Kebocoran oli bada beberapa bagian mesin?', 6, 6, 'ganti seal ori pabrikan motor pada bagian yang bocor', 0);
INSERT INTO `mst_gejala` VALUES (6, 'K2', 'Aki cepat habis atau drop', 7, 10, 'harus di cek pengkabelan karena sangat besar kemungkinan ada konsleting ', 0);
INSERT INTO `mst_gejala` VALUES (7, 'K2', 'Kabel Lampu Cepat Putus', 8, 10, 'bersihkan socket spull yang terhubung langsung ke socket lampu', 0);
INSERT INTO `mst_gejala` VALUES (8, 'K2', 'Motor Mati Tiba Tiba saat hujan', 9, 10, 'kop busi harus di ganti, kemungkinan juga hanya longgar, hingga busi kemasukan air', 0);
INSERT INTO `mst_gejala` VALUES (9, 'K2', 'Motor Mati total (lampu, stater, klakson)', 10, 10, 'bisa jadi aki habis, kiprok rusak atau kabel konslet', 0);
INSERT INTO `mst_gejala` VALUES (10, 'K3', 'Motor tidak bisa di stater', 11, 15, 'dinamo stater perlu di service', 0);
INSERT INTO `mst_gejala` VALUES (11, 'K3', 'Busi tidak mengeluarkan percikan api', 12, 15, 'spull pengisian kurang berkerja dengan baik', 0);
INSERT INTO `mst_gejala` VALUES (12, 'K3', 'Apakah kabel koil putus', 13, 15, 'ganti kabel koil', 0);
INSERT INTO `mst_gejala` VALUES (13, 'K3', 'CDI konslet', 14, 15, 'diusahakan ganti baru dan ori', 0);
INSERT INTO `mst_gejala` VALUES (14, 'K3', 'Sepul rusak atau ada yang terputus', 15, 15, 'lilit ulang/ beli baru', 0);
INSERT INTO `mst_gejala` VALUES (15, 'K4', 'Terdengar suara berisik bada bagian kiri mesin', 16, 0, 'spi magnet lepas', 0);
INSERT INTO `mst_gejala` VALUES (16, 'K4', 'terdengar kasar disebelah kanan saat nyala', 17, 0, 'karet kopling harus ganti', 0);
INSERT INTO `mst_gejala` VALUES (17, 'K4', 'roda tidak stabil ?', 18, 0, 'laher roda pecah/ velg geol (harus di press velg/setting ulang)', 0);
INSERT INTO `mst_gejala` VALUES (18, 'K4', 'ban kurang angin ?', 0, 0, 'isi angin', 0);

-- ----------------------------
-- Table structure for mst_kerusakan
-- ----------------------------
DROP TABLE IF EXISTS `mst_kerusakan`;
CREATE TABLE `mst_kerusakan`  (
  `id` int NOT NULL,
  `kode` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mst_kerusakan
-- ----------------------------
INSERT INTO `mst_kerusakan` VALUES (1, 'K1', 'Kerusakan Mesin');
INSERT INTO `mst_kerusakan` VALUES (2, 'K2', 'Kerusakan Kelistrikan');
INSERT INTO `mst_kerusakan` VALUES (3, 'K3', 'Kerusakan Pengapian');
INSERT INTO `mst_kerusakan` VALUES (4, 'K4', 'Kerusakan Kaki Kaki / Penggerak Roda');

-- ----------------------------
-- Table structure for trx_diagnosa
-- ----------------------------
DROP TABLE IF EXISTS `trx_diagnosa`;
CREATE TABLE `trx_diagnosa`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `motor` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `gejala` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of trx_diagnosa
-- ----------------------------
INSERT INTO `trx_diagnosa` VALUES (11, 'arvin', 'jupiter', '1,2,6,5');
INSERT INTO `trx_diagnosa` VALUES (12, 'Adi', 'Ninja', '1');
INSERT INTO `trx_diagnosa` VALUES (13, 'Arvin', 'Beat', '1,6,5');
INSERT INTO `trx_diagnosa` VALUES (14, 'Ariq', 'Beat Karbu', '1,2,6,7,8,9');
INSERT INTO `trx_diagnosa` VALUES (15, 'Indra', 'Beat', '6,7,8,9');
INSERT INTO `trx_diagnosa` VALUES (16, 'test', 'motor', '1,6,7,8,9');
INSERT INTO `trx_diagnosa` VALUES (17, 'Rizky', 'CBR', '1,6');
INSERT INTO `trx_diagnosa` VALUES (18, 'Akbar', 'Supra', '1,6,10');
INSERT INTO `trx_diagnosa` VALUES (19, 'Rizky', 'Honda CBR', '1,2,6,15,16');

SET FOREIGN_KEY_CHECKS = 1;
